-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "physics" library
local physics = require "physics"
physics.start()
physics.setGravity(0, 0)
local math = require "math"
--------------------------------------------

-- forward declarations and other locals
local screenW, screenH, halfW = display.actualContentWidth, display.actualContentHeight, display.contentCenterX
local score = 0
local scoreText
local hanzo
local hanzo2
local botTable = {}
local gameLoopTimer

function scene:create( event )

	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.

	local sceneGroup = self.view

	-- We need physics started to add bodies, but we don't want the simulaton
	-- running until the scene is on the screen.
	physics.start()
	physics.pause()


	-- create a grey rectangle as the backdrop
	-- the physical screen will likely be a different shape than our defined content area
	-- since we are going to position the background from it's top, left corner, draw the
	-- background at the real top, left corner.

	local background = display.newImageRect("background1.gif", display.actualContentWidth, display.actualContentHeight )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY

	options =
	{
		width = 786,
		height = 608,
		numFrames = 2,

		sheetContentWidth = 786,
		sheetContentHeight = 1216
	}

	scoreText = display.newText("Score: " .. score, 460,15, native.systemFont, 24)

	imageSheet = graphics.newImageSheet("HanzoSheet.png",options)

	hanzo = display.newImageRect(imageSheet, 1, 60, 60)
	hanzo.x = 0 
	hanzo.y = 275
	hanzo:toFront()
	hanzo2 = display.newImageRect(imageSheet, 2, 60, 60)
	hanzo2.x = 0 
	hanzo2.y = 275
	hanzo2.isVisible = false
	physics.addBody(hanzo, {radius=15, isSensor=true})
	hanzo.myName = "hanzo"

	hanzoLegs = display.newImageRect("HanzoLegs.png", 60, 60)
	hanzoLegs.x = 0 
	hanzoLegs.y = 275

	gameLoopTimer = timer.performWithDelay(2000, gameLoop, 0)

	function createBot()
		local newBot = display.newImageRect("Bot.png", 60, 80)
		table.insert(botTable,newBot)
		physics.addBody(newBot, "dynamic", {radius=30})
		newBot.class = "botclass"
		newBot.myName = "bot"

		local whereFrom = math.random(3)
		if(whereFrom==1) then
		newBot.x = 440
		newBot.y = 275
		newBot:setLinearVelocity(-30,0)
		elseif(whereFrom==2) then
		newBot.x = 440
		newBot.y = 175
		newBot:setLinearVelocity(-30,0)
		elseif(whereFrom==3) then
		newBot.x = 440
		newBot.y = 75
		newBot:setLinearVelocity(-30,0)
		end
	end

	local function onGlobalCollision(event)
		if (event.phase == "began") then
			local obj1 = event.object1
			local obj2 = event.object2
			if((obj1.myName=="arrow" and obj2.myName=="bot")or(obj1.myName=="bot" and obj2.myName == "arrow"))then
				display.remove(obj1)
				display.remove(obj2)

				for i = #botTable, 1, -1 do
					if(botTable[i]==obj1 or botTable[i]==obj2)then
						table.remove(botTable,i)
					end
				end
				score = score +100
				scoreText.text="Score: " .. score
			end
		end
	end
	Runtime:addEventListener("collision", onGlobalCollision)

	-- -- create a grass object and add physics (with custom shape)
	-- local grass = display.newImageRect( "grass.png", screenW, 82 )
	-- grass.anchorX = 0
	-- grass.anchorY = 1
	
	-- --  draw the grass at the very bottom of the screen
	-- grass.x, grass.y = display.screenOriginX, display.actualContentHeight + display.screenOriginY
	
	-- -- define a shape that's slightly shorter than image bounds (set draw mode to "hybrid" or "debug" to see)
	-- local grassShape = { -halfW,-34, halfW,-34, halfW,34, -halfW,34 }
	-- physics.addBody( grass, "static", { friction=0.3, shape=grassShape } )
	
	-- -- all display objects must be inserted into group
	-- sceneGroup:insert( background )
	-- sceneGroup:insert( grass)
end


function fireArrow(angle)
	newArrow = display.newImageRect("HanzoArrow.png", 60, 60)
	newArrow.x, newArrow.y, newArrow.rotation = hanzo.x, hanzo.y, angle
	physics.addBody(newArrow, "dynamic", {radius=2}, { isSensor=true })
	newArrow.isBullet = true
	newArrow.myName = "arrow"
	local direction = rotateTo({x=25,y=0},angle)
	newArrow:applyForce(direction.x, direction.y, newArrow.x, newArrow.y)
	hanzo2.isVisible = true
	hanzo.isVisible = false
	timer.performWithDelay(500,firingHanzo)
	hanzo:toFront()
	hanzo2:toFront()
end

function angleOfPoint( point )	
	local x, y = point.x, point.y	
	local radian = math.atan2(y,x)	
	local angle = radian*180/math.pi	
	if angle < 0 then 
		angle = 360 + angle 
	end		
	return angle
end

function angleBetweenPoints( a, b )	
	local x, y = b.x - a.x, b.y - a.y	
	return angleOfPoint( { x=x, y=y } )
end

function rotateTo( point, degrees )	
	local x, y = point.x, point.y	
	local theta = math.rad(degrees)	
	local z = {
		x = x * math.cos(theta) - y * math.sin(theta),
		y = x * math.sin(theta) + y * math.cos(theta)
		}	
	return z
end

function touch(e)
	local angle = angleBetweenPoints(hanzo,e)
	hanzo.rotation = angle
	hanzo2.rotation = angle
	if(e.phase=="ended") then
		fireArrow(angle)
	end
end

Runtime:addEventListener('touch', touch)

function firingHanzo(event)
		hanzo2.isVisible = false
		hanzo.isVisible = true
end

function updateText()
	scoreText.text = "Score: " .. score
end

function gameLoop()
	createBot()
	for i = #botTable, 1, -1 do
		local thisBot = botTable[i]
		if(thisBot.x < -100 or thisBot.x>580 or thisBot.y<-100 or thisBot.y>420) then
			display.remove (thisBot)
			table.remove(botTable,i)
		end
	end
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		physics.start()
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		physics.stop()
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local sceneGroup = self.view
	
	package.loaded[physics] = nil
	physics = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene